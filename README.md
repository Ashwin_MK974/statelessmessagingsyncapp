# Sync Stateless Messasing App



Application de chat de messagerie stateless en temps réel développée en utilisant Node.js, Socket.io et React. Elle permet aux utilisateurs de communiquer instantanément avec d'autres utilisateurs connectés la même instance de connexion.




Cette application est stateless car elle ne conserve pas les données une fois qu'elle est déconnectée.



Une version statefull de cette application en utilisant une base de donnée de type NoSQL comme MongoDB est pévue dans les versions suivantes afin de garder l'état des conservations des données.


### Visualisation de l'application
https://sync-messaging-amk.netlify.app/
