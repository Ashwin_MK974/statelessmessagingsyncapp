import "./App.css";
import { useState, useEffect } from "react";
import io from "socket.io-client";
import Message from "./components/Messages";
import { url } from "./url";
function App() {
  const [message, setMessage] = useState("");
  const [listMessage, setListMessage] = useState([]);
  const [username, setusername] = useState("");
  const [socket] = useState(() => io(url));
  const [next, setNext] = useState(false);
  useEffect(() => {
    socket.on("message_from_server", (data) => {
      console.log("from dest : " + data);
      setListMessage((prev) => [...prev, data]);
    });
    if (window.localStorage.username) {
      console.log("Username defined ...");
      setNext(true);
      setusername(window.localStorage.username);
    }
  }, []);
  useEffect(() => {
    const div = document.querySelector(".message-list");
    div.scrollTop = div.scrollHeight;
  }, [listMessage]);

  const handlerMessage = (e) => {
    e.preventDefault();
    console.log("Your Message : ", { username, message });
    socket.emit("message_from_client", { username, message });
    setListMessage((prev) => [...prev, { username, message }]);
    console.log("LisMessage : ", listMessage);
  };

  const connectionInit = (e) => {
    e.preventDefault();
    document.querySelector("input").value = "";
    window.localStorage.username = username;
    setNext(true);
  };
  const handleDisconnect = () => {
    window.localStorage.username = "";
    setusername("");
    setNext(false);
    document.querySelector("input").value = "";
  };
  return (
    <div className="main-container">
      <div className="scs">
        <div className="message-list">
          {listMessage.map((postMessage, index) => (
            <Message
              key={index}
              itemsData={postMessage}
              username={username}
              items={index}
            />
          ))}
        </div>
        {next ? (
          <form onSubmit={(e) => handlerMessage(e)}>
            <input
              className="msg"
              type="text"
              placeholder="Entrer Votre message"
              defaultValue={message}
              onChange={(e) => {
                setMessage(e.target.value);
              }}
              name="message"
              id="msg"
            />
            <input type="submit" value="Envoyer" />
            <div
              className="disconnect"
              onClick={(_) => {
                handleDisconnect();
              }}
            >
              Déconnecter
            </div>
          </form>
        ) : (
          <form
            action=""
            onSubmit={(e) => {
              connectionInit(e);
            }}
          >
            <input
              type="text"
              className="msg"
              name="username"
              id="usn"
              placeholder="Entrer votre nom utilisateur"
              onChange={(e) => {
                setusername(e.target.value);
              }}
            />
            <input type="submit" value="Valider" />
          </form>
        )}
      </div>
    </div>
  );
}

export default App;
