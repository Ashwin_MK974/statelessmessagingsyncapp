import React, { useState, useEffect } from "react";

const Messages = ({ itemsData, username, items }) => {
  const [Opacity, setOpacity] = useState(0);

  useEffect(() => {
    setTimeout(() => {
      setOpacity(1);
    }, 300);
  }, []);

  console.log("R ==> ", itemsData, "User : " + username, "index : " + items);
  let msgY = items * 60;
  const msgC =
    itemsData.username == username
      ? "msg-container msg-container-right"
      : "msg-container";
  const usernamePlace =
    itemsData.username == username ? "username username-right" : "username";
  return (
    <div className={msgC} style={{ top: msgY + "px", opacity: Opacity }}>
      <div className={usernamePlace} title={itemsData.username}>
        {itemsData.username[0]}
      </div>
      <p>{itemsData.message}</p>
    </div>
  );
};

export default Messages;
